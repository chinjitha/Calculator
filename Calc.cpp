#include<iostream>
#include "CalcLib.h"
int main()
{
    double a,b,rs1,rs2,rs3,rs4,rs5;
    int c,d;
    std::cout<<"......CALCULATOR.....\n"<<std::endl;
    std::cout<<"\nEnter first number\t"<<std::endl;
    std::cin>>a;
    std::cout<<"\nEnter second number\t"<<std::endl;
    std::cin>>b;
    rs1=Addition(a,b);
    rs2=Subtraction(a,b);
    rs3=Division(a,b);
    rs4=Multiplication(a,b);
    c=(int)a;
    d=(int)b;
    rs5=Modulus(c,d);
    std::cout<<"\n----Results----\n"<<std::endl;
    std::cout<<"\nSum\t\t"<<rs1<<"\nDifference\t"<<rs2<<"\nQuotient\t"<<rs3<<"\nProduct\t\t"<<rs4<<std::endl;
    std::cout<<"Remainder\t"<<rs5<<std::endl;
 }
